# App Ref. Cards

MIT License, Copyright (c) 2023, Berufsbildungsschule Winterthur, Rinaldo Lanza


# Übersicht
Die folgenden Beispiele/Apps dienen als Ausgangslage für die Erarbeitung möglicher Container Lösungen.
Die Beispiele sind sehr einfach gehalten und stellen exemplarische Anforderungen an die Laufzeitumgebung und
können im Unterricht erweitert werden.

Zu jeder Appp Ref. Card (Referenzkarte) existiert eine spezifische Dokumentation in den jeweiligen Git-Repositories.


## Architecture Ref. Card 01 / Spring Boot
Spring Boot Applikation als Website mit oder ohne API-Service. Z.B. als Website mit Statusinformation über das Netzwerk, Geo-Location, etc.

Anforderungen an die Cloud Lösung:
- Java Laufzeitumgebung
- Nginx als Webserver mit Weiterleitung an den internen Tomcat Port 8080
- Build Prozess mit Maven oder Gradle, Artefakt: JAR-Datei

Link zum Repository:<br/>
https://gitlab.com/bbwrl/m347-ref-card-01



## Architecture Ref. Card 02 / React
Einfache React-App ohne weitere Ressourcen. Die App könnte ausgebaut werden, mit der Anzeige 
bspw. Wetter-Daten oder einer anderen interessanten API.

Anforderungen an die Cloud Lösung:
- Simpler Webserver, z.B. Nginx
- Build Prozess mit Node/npm, Artefakt: Build Ordner mit HTML und JavaScript

Link zum Repository:<br/>
https://gitlab.com/bbwrl/m347-ref-card-02



## Architecture Ref. Card 08 / Spring Boot
Einfache App mit einem "light" und einem "dark" Theme, welche über Umgebungsvariablen gesetzt werden können.
Die App bietet zudem ein Re-Captcha für welches das Secret über eine Umgebungsvariable angegeben wird.

Anforderungen:
- Umgang mit Umgebungsvariablen

Link zum Repository:<br/>
https://gitlab.com/peter.rutschmann/m347-ref-card-08